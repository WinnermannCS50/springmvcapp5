package org.example.Lesson20.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
/**
 * При GET--запросе на /first/hello сработает метод helloPage()
 * При GET--запросе на /first/goodbye сработает метод goodByePage()
 */
@RequestMapping("/first")
public class FirstController {

    /**
     * При GET--запросе на /hello сработает метод helloPage()
     * @return
     *
     * HttpServletRequest request - принимает все сведения об Http-запросе пользователя
     *
     * Делаем в браузере HTTP GET-запрос с параметрами name=Tom и surname=Jones после знака ?
     * http://localhost:8083/SpringMVCApp4_2_war_exploded/first/hello?name=Tom&surname=Jones
     *
     * @RequestParam(value = "name", required = false) String name, @RequestParam(value = "surname", required = false) String surname)
     * @RequestParam - требует обязательного введения параметров и ложит их в String name и String surname
     * required = false - позволяет оставлять параметры String name и String surname пустыми
     *
     * Model - передает параметры ?name=Tom&surname=Jones Http-запроса в представление first/hello.html и отображает их пользователю
     * <p th:text="${message}"/> - отображает на WEB-странице то что передано в model.addAttribute("message", "Hello " + name + " " + surname);
     */
    @GetMapping("/hello")
    public String helloPage(@RequestParam(value = "name", required = false) String name, @RequestParam(value = "surname", required = false) String surname, Model model){

        //System.out.println("Hello " + name + " " + surname);

        /**
         * Ложим в model ключ: "message", значение: "Hello " + name + " " + surname
         */
        model.addAttribute("message", "Hello " + name + " " + surname);
        return "first/hello";

    }

    /**
     * При GET--запросе на /goodbye сработает метод goodByePage()
     * @return
     */
    @GetMapping("/goodbye")
    public String goodByePage(){
        return "first/goodbye";

    }
}
